# README Star Wars backend-api #

Trello Acitivities: https://trello.com/b/pVRw3YTm/starwars-project

### Java Techs that I have used on this project: ###

* Spring Boot

I used it with your default configuration.
To run just use the command mvn spring-boot:run

* Liquibase 

I used as .xml configuration the file is in http://tiny.cc/8mv7by 

* Swagger

After tomcat starts you can access http://localhost:8080/swagger-ui.html 
To see or to know how to integrate with our api.

### To run just mvn spring-boot:run ###
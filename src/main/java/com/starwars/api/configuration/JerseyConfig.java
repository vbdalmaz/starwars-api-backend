package com.starwars.api.configuration;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.wadl.internal.WadlResource;

import com.starwars.api.rest.CharacterRest;
import com.starwars.api.rest.ScriptRest;
import com.starwars.api.rest.SettingsRest;

public class JerseyConfig extends ResourceConfig{

	public JerseyConfig() {
		registerEndpoints();
	}

	private void registerEndpoints() {
		register(WadlResource.class);
		register(ScriptRest.class);
		register(CharacterRest.class);
		register(SettingsRest.class);
	}
}
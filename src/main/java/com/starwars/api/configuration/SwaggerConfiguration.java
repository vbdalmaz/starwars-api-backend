package com.starwars.api.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfiguration {
    
	@Bean
    Docket rsApi() {
        return new Docket(DocumentationType.SWAGGER_12)
            .apiInfo(apiInfo())
            .select()
                    .apis(RequestHandlerSelectors.basePackage("com.starwars.api"))
            .paths(PathSelectors.any())
            .build()
            .pathMapping("/")
            .useDefaultResponseMessages(false);
    }

    private ApiInfo apiInfo() {
       return new ApiInfoBuilder()
            .title("StarWars API")
            .description("StarWars API")
            .version("0.0.1")
            .termsOfServiceUrl("")
            .contact("Lucasfilm Code Company")
            .license("Private")
            .licenseUrl("https://pt.wikipedia.org/wiki/Star_Wars")
            .build();
    }
}
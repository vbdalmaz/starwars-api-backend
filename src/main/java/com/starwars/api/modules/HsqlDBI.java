package com.starwars.api.modules;

import org.skife.jdbi.v2.DBI;
import org.springframework.stereotype.Component;

@Component
public class HsqlDBI {

	private DBI dbi;

	public HsqlDBI() {
		//by default
	}

	public HsqlDBI(DBI dbi) {
		dbi.registerArgumentFactory(new ListArgumentFactory());
		this.dbi = dbi;
	}

	public <SqlObjectType> SqlObjectType onDemand(Class<SqlObjectType> sqlObjectType) {
		return dbi.onDemand(sqlObjectType);
	}
}

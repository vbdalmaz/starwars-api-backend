package com.starwars.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.starwars.api.Application;
import com.starwars.api.dao.CharacterDAO;
import com.starwars.api.model.Character;
import com.starwars.api.modules.HsqlDBI;

@Service
public class CharacterController {
	private final CharacterDAO characterDAO;
	private HsqlDBI hsqlDBI;
	
	@Autowired
	public CharacterController(Application application){
		if(hsqlDBI == null){
			hsqlDBI = application.getHsqlDBI();
		}
		this.characterDAO = hsqlDBI.onDemand(CharacterDAO.class);
	}
	
	public Character getById(Integer id) {
		return characterDAO.getById(id);
	}
	
	public Character getByName(String name) {
		return characterDAO.getByName(name);
	}
	
	public Character insert(Character character) {
		Character characterExistance = getByName(character.getName());
		
		if(characterExistance == null){
			characterDAO.insert(character);
		}else{
			return characterExistance;
		}
		
		return getByName(character.getName());
	}
	
	
	public Boolean exists(Character character) {
		return characterDAO.exist(character.getId()) > 0; 
	}

	public List<Character> getAll() {
		return characterDAO.getAll();
	}
}
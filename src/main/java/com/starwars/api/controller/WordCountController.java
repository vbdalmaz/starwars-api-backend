package com.starwars.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.starwars.api.Application;
import com.starwars.api.dao.WordCountDAO;
import com.starwars.api.facade.WordFacade;
import com.starwars.api.modules.HsqlDBI;

@Service
public class WordCountController {
	
	private final WordCountDAO wordCountDAO;
	private HsqlDBI hsqlDBI;
	
	@Autowired
	public WordCountController(Application application){
		if(hsqlDBI == null){
			hsqlDBI = application.getHsqlDBI();
		}
		this.wordCountDAO = hsqlDBI.onDemand(WordCountDAO.class);
	}

	public void insert(Integer settingId, Integer characterId, String word) {
		wordCountDAO.insert(settingId, characterId, word);
	}

	public List<Integer> getCharactersInSettting(Integer settingId) {
		 return wordCountDAO.getCharacterInSetting(settingId);
	}
	
	public List<WordFacade> getWordCountByCharacter(Integer characterId) {
		 return wordCountDAO.getWordCountByCharacter(characterId);
	}
}

package com.starwars.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.starwars.api.Application;
import com.starwars.api.dao.SettingsDAO;
import com.starwars.api.model.Setting;
import com.starwars.api.modules.HsqlDBI;

@Service
public class SettingsController {
	
	private final SettingsDAO settingsDAO;
	private HsqlDBI hsqlDBI;
	
	@Autowired
	public SettingsController(Application application){
		if(hsqlDBI == null){
			hsqlDBI = application.getHsqlDBI();
		}
		this.settingsDAO = hsqlDBI.onDemand(SettingsDAO.class);
	}
	
	public Setting getById(Integer id) {
		return settingsDAO.getById(id);
	}
	
	public Setting getByName(String name) {
		return settingsDAO.getByName(name);
	}
	
	public Setting insert(Setting setting) {
		Setting settingExistance = getByName(setting.getName());
		if(settingExistance == null){
			settingsDAO.insert(setting);
		}else{
			return settingExistance;
		}
		
		return getByName(setting.getName());
	}
	
	public Boolean exists(Setting setting) {
		return settingsDAO.exist(setting.getId()) > 0; 
	}

	public List<Setting> getAll() {
		return settingsDAO.getAll();
	}
}
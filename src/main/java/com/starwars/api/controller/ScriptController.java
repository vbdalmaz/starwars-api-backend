package com.starwars.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.starwars.api.Application;
import com.starwars.api.constant.ScriptUtil;
import com.starwars.api.constant.StarWarsApiMessages;
import com.starwars.api.dao.ScriptDAO;
import com.starwars.api.exception.StarWarsException;
import com.starwars.api.model.Script;
import com.starwars.api.modules.HsqlDBI;

@Service
public class ScriptController {
	private final ScriptDAO scriptDAO;
	private HsqlDBI hsqlDBI;
	
	@Autowired
	public ScriptController(Application application){
		if(hsqlDBI == null){
			hsqlDBI = application.getHsqlDBI();
		}
		this.scriptDAO = hsqlDBI.onDemand(ScriptDAO.class);

	}
	
	public void insert(Script script){
		scriptDAO.insert(script);
	}

	public boolean existScript(String script) throws StarWarsException{
		String[] lines = script.split(System.getProperty("line.separator"));
		Script script2 = new Script();
		
		int lineRetry = 20;
		
		for(String line : lines){
			if(line.trim().startsWith(ScriptUtil.NameOfScript)){
				script2.setName(line.trim());
				break;
			}			
			if(lineRetry-- == 0 && script2.getName() == null){
				throw new StarWarsException(500, StarWarsApiMessages.API_INTERNAL_ERROR);
			}
		}
		
		if(scriptDAO.exist(script2.getName()) != 0){
			return true;
		}else{
			insert(script2);
			return false;
		}
	}
}
package com.starwars.api.facade;

import java.util.List;

import com.starwars.api.model.Setting;

public class SettingsFacade {
	
	private Integer id;
	private String name;
	private List<CharacterFacade> characters;
	
	public SettingsFacade(){
		
	}
	
	public SettingsFacade(Setting setting, List<CharacterFacade> characters){
		this.id = setting.getId();
		this.name = setting.getName();
		this.characters = characters;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<CharacterFacade> getCharacters() {
		return characters;
	}
	public void setCharacters(List<CharacterFacade> characters) {
		this.characters = characters;
	}
}
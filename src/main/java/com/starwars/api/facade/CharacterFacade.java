package com.starwars.api.facade;

import java.util.List;
import com.starwars.api.model.Character;

public class CharacterFacade {
	
	private Integer id;
	private String name;
	private List<WordFacade> wordCounts;
	
	public CharacterFacade(){
		
	}
	
	public CharacterFacade(Integer id, String name, List<WordFacade> wordCounts) {
		super();
		this.id = id;
		this.name = name;
		this.wordCounts = wordCounts;
	}
	
	
	public CharacterFacade(Character character, List<WordFacade> wordCounts) {
		super();
		this.id = character.getId();
		this.name = character.getName();
		this.wordCounts = wordCounts;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<WordFacade> getWordCounts() {
		return wordCounts;
	}
	public void setWordCounts(List<WordFacade> wordCounts) {
		this.wordCounts = wordCounts;
	}
}

package com.starwars.api.facade;

public class WordFacade {
	
	private String word;
	private Integer count;
	
	public WordFacade() {

	}
	
	public WordFacade(String word, Integer count) {
		super();
		this.word = word;
		this.count = count;
	}
	
	
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
}

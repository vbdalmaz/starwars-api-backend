package com.starwars.api.runnable;

import java.util.regex.Matcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.starwars.api.Application;
import com.starwars.api.constant.ScriptUtil;
import com.starwars.api.controller.CharacterController;
import com.starwars.api.controller.ScriptController;
import com.starwars.api.controller.SettingsController;
import com.starwars.api.controller.WordCountController;
import com.starwars.api.model.Character;
import com.starwars.api.model.Setting;

@Component
public class ParseScript implements Runnable {
	
	private final CharacterController characterController;
	private final SettingsController settingsController;
	private final WordCountController wordCountController;
	private String script;
	
	private Setting settingParser;
	private Character characterParser;
	
	@Autowired
	public ParseScript(Application application,  CharacterController characterController,
			ScriptController scriptController, SettingsController settingsController, WordCountController wordCountController
			) {
		this.characterController = characterController;
		this.settingsController = settingsController;
		this.wordCountController = wordCountController;
	}
	
	public void setScript(String script){
		this.script = script;
	}


	@Override
	public void run() {
		String[] lines = script.split(System.getProperty("line.separator"));
		
		for(String line: lines){
			if(isInitialOfSetting(line)){
				createSettings(line);
			}
			else if(isInitialOfCharacter(line) && settingParser != null){
				createCharacter(line);
			}else if(isInitialOfDialogue(line) && settingParser != null && characterParser != null){
				createDialogue(line);
			}
			
		}
	}
	
	private Boolean isInitialOfSetting(String line){
		return line.startsWith(ScriptUtil.SettingsInitial1) || line.startsWith(ScriptUtil.SettingsInitial2);
	}
	
	private Boolean isInitialOfCharacter(String line){
		Matcher matcher = ScriptUtil.CharacterPattern.matcher(line);
		
		return matcher.find();
	}
	
	private Boolean isInitialOfDialogue(String line){
		Matcher matcher = ScriptUtil.DialoguePattern.matcher(line);
		
		return matcher.find();
	}
	
	private void createSettings(String line){
		Setting setting = new Setting();
		
		for(String settingName: line.split(" - ")){
			setting.setName(settingName.replace(ScriptUtil.SettingsInitial2, "").replace(ScriptUtil.SettingsInitial1, ""));
		    break;
		}
		
		settingParser = settingsController.insert(setting);
	}
	
	private void createCharacter(String line){
		Character character = new Character();
		character.setName(line.trim());
		  
		characterParser = characterController.insert(character);
	}
	
	private void createDialogue(String line){
		String[] words = line.toLowerCase().trim().split("[\\W\\d]+");
		for(String word: words){
			if(settingParser != null && settingParser.getId() != null 
					& characterParser !=null && characterParser.getId() != null 
						&& word != null && !word.isEmpty())
				
			wordCountController.insert(settingParser.getId(), characterParser.getId(), word);
		}
	}
}
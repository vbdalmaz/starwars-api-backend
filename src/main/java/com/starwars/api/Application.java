package com.starwars.api;

import java.sql.Connection;
import java.util.Arrays;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.logging.PrintStreamLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.starwars.api.configuration.ApplicationConfig;
import com.starwars.api.modules.HsqlDBI;

import liquibase.Liquibase;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

@ComponentScan(basePackages = {"com.starwars.api.runnable", "com.starwars.api.controller","com.starwars.api.rest","com.starwars.api.configuration","com.starwars.api.modules"})
@SpringBootApplication
public class Application {

	@Autowired
	private ApplicationConfig applicationConfig; 
	
	@Autowired
	private HsqlDBI hsqlDBI;
	
	private Handle handler;
	
	public static void main(String[] args) throws LiquibaseException {
	    final Connection connection = new Application().getConnection().getConnection();
	    
	    final Liquibase migrator = new Liquibase("db.changelog-master.xml", new ClassLoaderResourceAccessor(), new JdbcConnection(
	            connection));
	        migrator.dropAll();
	        migrator.update("");
	        
	        ApplicationContext ctx = SpringApplication.run(Application.class, args);
	        
	        String[] beanNames = ctx.getBeanDefinitionNames();
	        Arrays.sort(beanNames);
	        for (String beanName : beanNames) {
	            System.out.println(beanName);
	        }
	}
	
	@Bean
	public Handle getConnection(){
	    if(handler == null) {
	        final DBI dbi = new DBI("jdbc:h2:mem:test;");
	        dbi.setSQLLog(new PrintStreamLog(System.out));
        handler = dbi.open();
	        this.hsqlDBI = new HsqlDBI(dbi);
	      }
	    
	      return handler;
	}
	
	public HsqlDBI getHsqlDBI(){
		if(handler == null){
			handler = getConnection();
		}
		
		return this.hsqlDBI;
	}
}
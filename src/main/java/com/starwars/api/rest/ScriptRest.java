package com.starwars.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.starwars.api.constant.StarWarsApiMessages;
import com.starwars.api.controller.ScriptController;
import com.starwars.api.runnable.ParseScript;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api("Script")
@RestController
public class ScriptRest {

	private final ScriptController scriptController;
	private final ParseScript parse;

	@Autowired
	public ScriptRest(final ScriptController scriptController, final ParseScript parse) {
		this.scriptController = scriptController;
		this.parse = parse;

	}

	@Consumes({ MediaType.TEXT_PLAIN })
	@Produces({ MediaType.TEXT_PLAIN })
	@RequestMapping(value = "/script", method = RequestMethod.POST)
	@ApiOperation("Route to receive the movie script")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Swagger Does not show 200! Movie script successfully received"),
			@ApiResponse(code = 200, message = "Movie script successfully received"),
			@ApiResponse(code = 403, message = "Movie script already received"),
			@ApiResponse(code = 500, message = "Unexpected error") })
	public ResponseEntity<String> postScript(@RequestBody String script) {
		try {
			if (script == null || script.trim().isEmpty()) {
				return new ResponseEntity<String>("message:" + StarWarsApiMessages.API_INTERNAL_ERROR,
						HttpStatus.INTERNAL_SERVER_ERROR);
			}

			if (scriptController.existScript(script)) {
				return new ResponseEntity<String>("message:" + StarWarsApiMessages.SCRIPT_DUPLICATED,
						HttpStatus.FORBIDDEN);
			}
			
			parse.setScript(script);
			parse.run();

			return new ResponseEntity<String>("message:" + StarWarsApiMessages.SCRIPT_CREATED, HttpStatus.OK);
		} catch (Exception exception) {
			
			exception.printStackTrace();
			
//			return new ResponseEntity<String>("message:" + StarWarsApiMessages.API_INTERNAL_ERROR,
//					HttpStatus.INTERNAL_SERVER_ERROR);
			
			return new ResponseEntity<String>("message:" + exception.getMessage(),
					HttpStatus.INTERNAL_SERVER_ERROR);
			
			
		}
	}
}
package com.starwars.api.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.starwars.api.constant.StarWarsApiMessages;
import com.starwars.api.controller.CharacterController;
import com.starwars.api.controller.WordCountController;
import com.starwars.api.facade.CharacterFacade;
import com.starwars.api.model.Character;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api("Character")
@RestController
public class CharacterRest {

	private final CharacterController characterController;
	private final WordCountController wordCountController;

	@Autowired
	public CharacterRest(final CharacterController characterController, WordCountController wordCountController) {
		this.characterController = characterController;
		this.wordCountController = wordCountController;
	}

	@Produces({ MediaType.APPLICATION_JSON })
	@RequestMapping(value = "/characters", method = RequestMethod.GET)
	@ApiOperation("Route to return all characters")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Object Json"),
			@ApiResponse(code = 500, message = "Unexpected error") })
	public ResponseEntity<String> getAllCharacters() {
		try {
			
			List<Character> listOfCharacters = characterController.getAll();
			List<CharacterFacade> listOfCharactersFacade = new ArrayList<CharacterFacade>();
			
			for(Character character: listOfCharacters){
				listOfCharactersFacade.add(new CharacterFacade(characterController.getById(character.getId()), wordCountController.getWordCountByCharacter(character.getId())));
			}
			
			
			return new ResponseEntity<String>(new Gson().toJson(characterController.getAll()), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("message:" + StarWarsApiMessages.API_INTERNAL_ERROR,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Produces({ MediaType.APPLICATION_JSON })
	@RequestMapping(value = "/characters/{id}", method = RequestMethod.GET)
	@ApiOperation("Route to return a character by id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Object Json"),
			@ApiResponse(code = 404, message = "Movie setting with id {id} not found"),
			@ApiResponse(code = 500, message = "Unexpected error") })
	public ResponseEntity<String> getCharacterById(@PathVariable("id") Integer id) {
		try {

			Character character = characterController.getById(id);

			if (character == null) {
				return new ResponseEntity<String>(
						"message:" + String.format(StarWarsApiMessages.CHARACTER_NOT_FOUND.getMessage(), id),
						HttpStatus.NOT_FOUND);
			}
			CharacterFacade characterFacade = new CharacterFacade(character,
					wordCountController.getWordCountByCharacter(id));

			return new ResponseEntity<String>(new Gson().toJson(characterFacade), HttpStatus.OK);

		} catch (

		Exception e) {
			return new ResponseEntity<String>("message:" + StarWarsApiMessages.API_INTERNAL_ERROR,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
package com.starwars.api.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.starwars.api.constant.StarWarsApiMessages;
import com.starwars.api.controller.CharacterController;
import com.starwars.api.controller.SettingsController;
import com.starwars.api.controller.WordCountController;
import com.starwars.api.facade.CharacterFacade;
import com.starwars.api.facade.SettingsFacade;
import com.starwars.api.model.Setting;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api("Settings")
@RestController
public class SettingsRest {
	
	private final SettingsController settingsController;
	private final CharacterController characterController;
	private final WordCountController wordCountController;
	
	
	@Autowired
	public SettingsRest(final SettingsController settingsController,final CharacterController characterController,
			final WordCountController wordCountController) {
		
		this.settingsController = settingsController;
		this.characterController = characterController;
		this.wordCountController = wordCountController;
		
	}

	@Produces({ MediaType.APPLICATION_JSON })
	@RequestMapping(value = "/settings", method=RequestMethod.GET)
	@ApiOperation("Route to return all movie settings")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Object Json"),
			@ApiResponse(code = 500, message = "Unexpected error") })
	public ResponseEntity<String> getAllSettings() {
		try{
			
			List<SettingsFacade> settingListFacade = new  ArrayList<SettingsFacade>();
			List<Setting> settingList = settingsController.getAll();
			
			for(Setting setting: settingList){
			
				List<Integer> listOfIdCharacters = wordCountController.getCharactersInSettting(new Integer(setting.getId()));
				List<CharacterFacade> listOfCharacters = new ArrayList<CharacterFacade>();
				
				for(Integer characterId: listOfIdCharacters){
					listOfCharacters.add(new CharacterFacade(characterController.getById(characterId), wordCountController.getWordCountByCharacter(characterId)));
				}
		
				settingListFacade.add(new SettingsFacade(setting, listOfCharacters));
				
			}
			
			
			return new ResponseEntity<String>(new Gson().toJson(settingListFacade), HttpStatus.OK);
		}catch(Exception e){
			return new ResponseEntity<String>("message:" + StarWarsApiMessages.API_INTERNAL_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@Produces({ MediaType.APPLICATION_JSON })
	@RequestMapping(value = "/settings/{id}", method=RequestMethod.GET)
	@ApiOperation("Route to return a movie setting by it id")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Object Json"),
			@ApiResponse(code = 404, message = "Movie setting with id {id} not found"),
			@ApiResponse(code = 500, message = "Unexpected error") })
	public ResponseEntity<String> get(@PathVariable("id") String id) {
		try{
			
			Setting setting = settingsController.getById(new Integer(id));
			
			if(setting != null){
				List<Integer> listOfIdCharacters = wordCountController.getCharactersInSettting(new Integer(id));
				List<CharacterFacade> listOfCharacters = new ArrayList<CharacterFacade>();
				
				for(Integer characterId: listOfIdCharacters){
					listOfCharacters.add(new CharacterFacade(characterController.getById(characterId), wordCountController.getWordCountByCharacter(characterId)));
				}
		
				SettingsFacade settingsFacade = new SettingsFacade(setting, listOfCharacters);
				
				return new ResponseEntity<String>(new Gson().toJson(settingsFacade), HttpStatus.OK);
				
			}
		
			return new ResponseEntity<String>("message:" +  String.format(StarWarsApiMessages.SETTINGS_NOT_FOUND.getMessage(), id), HttpStatus.NOT_FOUND);
			
			
		}catch(Exception e){
			return new ResponseEntity<String>("message:" + StarWarsApiMessages.API_INTERNAL_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
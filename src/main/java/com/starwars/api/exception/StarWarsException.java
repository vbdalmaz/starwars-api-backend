package com.starwars.api.exception;

import com.starwars.api.constant.StarWarsApiMessages;

public class StarWarsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4849682793722021521L;

	private Integer httpCode;
	private String errorType;
	private String message;

	public StarWarsException(Integer httpCode, StarWarsApiMessages starWarsApiMessages) {
		super(starWarsApiMessages.getMessage());
		this.setMessage(starWarsApiMessages.getMessage());
		this.setHttpCode(httpCode);
		this.setErrorType(starWarsApiMessages.toString());
	}

	public Integer getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(Integer httpCode) {
		this.httpCode = httpCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
}
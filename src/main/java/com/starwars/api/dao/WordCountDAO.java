package com.starwars.api.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.starwars.api.facade.WordFacade;

public abstract class WordCountDAO {
	@SqlUpdate("INSERT INTO WORDS_COUNT (ID_SETTING,ID_CHARACTER,WORD) VALUES (:settingId, :characterId, :word)")
	public abstract int insert(@Bind("settingId") Integer settingId, @Bind("characterId") Integer characterId,
			@Bind("word") String word);

	@SqlQuery("SELECT count(*) from WORDS_COUNT WHERE ID_SETTING = :settingId AND ID_CHARACTER = :characterId AND WORD = :word")
	public abstract int count(@Bind("settingId") Integer settingId, @Bind("characterId") Integer characterId,
			@Bind("word") String word);

	@SqlQuery("SELECT COUNT(WORD) AS QUANTITY, WORD from WORDS_COUNT WHERE ID_CHARACTER = :characterId GROUP BY WORD ORDER BY QUANTITY DESC LIMIT 10")
	@Mapper(WordCountMapper.class)
	public abstract java.util.List<WordFacade> getWordCountByCharacter(@Bind("characterId") Integer characterId);

	@SqlQuery("SELECT ID_CHARACTER from WORDS_COUNT WHERE ID_SETTING = :settingId GROUP BY ID_CHARACTER")
	@Mapper(ListMapper.class)
	public abstract java.util.List<Integer> getCharacterInSetting(@Bind("settingId") Integer settingId);

	public abstract void close();

	public static class WordCountMapper implements ResultSetMapper<WordFacade> {

		@Override
		public WordFacade map(int i, ResultSet rs, StatementContext statementContext) throws SQLException {
			final WordFacade wordFacade = new WordFacade();

			wordFacade.setWord(rs.getString("WORD"));
			wordFacade.setCount(rs.getInt("QUANTITY"));

			return wordFacade;
		}
	}

	public static class ListMapper implements ResultSetMapper<Integer> {

		@Override
		public Integer map(int i, ResultSet rs, StatementContext statementContext) throws SQLException {
			final Integer list = new Integer(rs.getInt("ID_CHARACTER"));

			return list;
		}
	}
}
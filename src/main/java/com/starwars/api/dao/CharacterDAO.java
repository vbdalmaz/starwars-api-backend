package com.starwars.api.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.starwars.api.model.Character;

public abstract class CharacterDAO {
	@SqlUpdate("INSERT INTO CHARACTERS (NAME) VALUES (:character.name)")
	public abstract int insert(@BindBean("character") Character character);

	@SqlQuery("SELECT count(*) from CHARACTERS WHERE ID = :characterId")
	public abstract int exist(@Bind("characterId") Integer characterId);

	@SqlQuery("SELECT ID, NAME FROM CHARACTERS WHERE ID = :characterId")
	@Mapper(CharacterMapper.class)
	public abstract Character getById(@Bind("characterId") Integer characterId);
	
	@SqlQuery("SELECT ID, NAME FROM CHARACTERS")
	@Mapper(CharacterMapper.class)
	public abstract List<Character> getAll();
	
	@SqlQuery("SELECT ID, NAME FROM CHARACTERS WHERE name = :characterName")
	@Mapper(CharacterMapper.class)
	public abstract Character getByName(@Bind("characterName") String name);

	public abstract void close();

	public static class CharacterMapper implements ResultSetMapper<Character> {
		@Override
		public Character map(int i, ResultSet rs, StatementContext statementContext) throws SQLException {

			return new Character(rs.getInt("ID"), rs.getString("NAME"));
		}
	}
}
package com.starwars.api.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.starwars.api.model.Script;

public abstract class ScriptDAO {
	
	  @SqlUpdate("INSERT INTO SCRIPT (name) VALUES (:script.name)")
	  public abstract int insert (@BindBean("script") Script script);

	  @SqlQuery("SELECT count(*) from SCRIPT WHERE name = :name")
	  public abstract int exist(@Bind("name") String name);

	  public abstract void close();

	  public static class ScriptMapper implements ResultSetMapper<Script> {

	    @Override
	    public Script map(int i, ResultSet rs, StatementContext statementContext) throws SQLException {
	      return new Script(rs.getInt("ID"), rs.getString("NAME"));
	    }
	  }
}
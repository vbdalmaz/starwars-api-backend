package com.starwars.api.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.starwars.api.model.Setting;

public abstract class SettingsDAO {
	@SqlUpdate("INSERT INTO SETTINGS (NAME) VALUES (:setting.name)")
	public abstract int insert(@BindBean("setting") Setting setting);

	@SqlQuery("SELECT count(*) from SETTINGS WHERE ID = :settingId")
	public abstract int exist(@Bind("settingId") Integer settingId);

	@SqlQuery("SELECT ID, NAME FROM SETTINGS WHERE ID = :settingId")
	@Mapper(SettingMapper.class)
	public abstract Setting getById(@Bind("settingId") Integer settingId);
	
	@SqlQuery("SELECT ID, NAME FROM SETTINGS")
	@Mapper(SettingMapper.class)
	public abstract List<Setting> getAll();

	@SqlQuery("SELECT ID, NAME FROM SETTINGS WHERE name = :settingName")
	@Mapper(SettingMapper.class)
	public abstract Setting getByName(@Bind("settingName") String name);

	public abstract void close();

	public static class SettingMapper implements ResultSetMapper<Setting> {
		@Override
		public Setting map(int i, ResultSet rs, StatementContext statementContext) throws SQLException {

			return new Setting(rs.getInt("ID"), rs.getString("NAME"));
		}
	}
}
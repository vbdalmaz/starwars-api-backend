package com.starwars.api.constant;

public class StarWarsApiRoutes {
	public static final String SCRIPT_POST = new String ("/script");
	public static final String SETTINGS_GET = new String ("/settings");
	public static final String SETTINGS_GET_BY_ID = new String (StarWarsApiRoutes.SETTINGS_GET + "/{id}");
	public static final String CHARACTER_GET = new String ("/characters");
	public static final String CHARACTER_GET_BY_ID = new String (StarWarsApiRoutes.CHARACTER_GET+"/{id}");
}
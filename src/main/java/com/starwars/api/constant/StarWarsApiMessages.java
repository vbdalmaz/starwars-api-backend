package com.starwars.api.constant;

public enum StarWarsApiMessages {
	
	  API_INTERNAL_ERROR("Unexpected error"),
	  SCRIPT_DUPLICATED("Movie script already received"),
	  SCRIPT_CREATED("Movie script successfully received"),  
	  SETTINGS_NOT_FOUND("Movie setting with id %d not found"),
	  CHARACTER_NOT_FOUND("Movie character with id %d not found");
	  
	  private final String message;

	  StarWarsApiMessages(String message){
	    this.message = message;
	  }

	  public String getMessage() {
	    return message;
	  }
}
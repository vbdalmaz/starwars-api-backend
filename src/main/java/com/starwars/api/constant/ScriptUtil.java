package com.starwars.api.constant;

import java.util.regex.Pattern;


public class ScriptUtil {
	public static final String NameOfScript = "Episode";
	public static final String SettingsInitial1 = "INT.";
	public static final String SettingsInitial2 = "INT./EXT.";
	public static final Pattern CharacterPattern = Pattern.compile("^[\\s]{22}[A-Z]");
	public static final Pattern DialoguePattern = Pattern.compile("^[\\s]{10}[A-Z]");
}
